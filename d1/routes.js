const http = require('http');

//Create a variable "port" to store the port number 
const port = 4000;

// Create a variable 'server' that stores the output of the 'create server' method

const server = http.createServer((req, res) => {
      //http://localhost:4000/greeting
      if(req.url =='/greeting'){
         res.writeHead(200,{'Content-Type': 'text/plain'})
         res.end('hello again')
      }

       else if(req.url =='/homepage'){
         res.writeHead(200,{'Content-Type': 'text/plain'})
         res.end('this is the homepage with a 200 status')
      }else{
      res.writeHead(404,{'Content-Type': 'text/plain'})
         res.end('Page not found')
       }


})

//Use the 'server' and 'port' variables created above 

server.listen(port);

console.log(`Server now accessible at localhost:${port}`)

//http://localhost:4000/home/about/contacts